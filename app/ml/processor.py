'''
 On demand interface based on ML (created by Eli/Polo)
 Just leaving here in case a customer requires this
'''

import numpy as np
import spacy
import random


class ResponseProcessor(object):
    def __init__(self, params, index):
        #required args intents, words, 
        reqs = ("intents.json", "words.pkl", "tags.pkl", "chatbot_model.h5")
        for arg in reqs:
            if arg not in params:
                raise Exception("Missing required object %s" % arg)
        
        self.intents = params["intents.json"]
        self.words = params["words.pkl"]
        self.tags = params["tags.pkl"]
        self.model = params["chatbot_model.h5"]
        self.npl = spacy.load('en_core_web_sm')
        self.index = index


    def chatbot_response(self, msg):
        ints = self.predict_class(msg)
        res = self.getResponse(ints, self.intents)
        return res

    # return bag of words array: 0 or 1 for each word in the bag that exists in the sentence
    def bow(self, sentence, words, show_details=False):
        # tokenize the pattern
        sentence_words = self.clean_up_sentence(sentence)
        # bag of words - matrix of N words, vocabulary matrix
        bag = [0]*len(words)
        for s in sentence_words:
            for i, w in enumerate(words):
                if w == s:
                    # assign 1 if current word is in the vocabulary position
                    bag[i] = 1
                    if show_details:
                        print ("found in bag: %s" % w)
        return(np.array(bag))

    def predict_class(self, sentence):
        # filter out predictions below a threshold
        p = self.bow(sentence, self.words)
        res = self.model.predict(np.array([p]))[0]
        ERROR_THRESHOLD = 0.25
        results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
        # sort by strength of probability
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append({"intent": self.tags[r[0]], "probability": str(r[1])})
        return return_list

    def clean_up_sentence(self, sentence):
        response = []
        for token in self.npl(sentence):
            if token.lemma_ != "-PRON-":
                response.append(token.lemma_.lower())
            else:
                response.append(token.text.lower())
        return response

    def getResponse(self, ints, intents_json):
        tag = ints[0]['intent']
        list_of_intents = intents_json['intents']
        for i in list_of_intents:
            if(i['tag']== tag):
                # not supported action random response
                if tag not in self.index:
                    result = random.choice(i['responses'])
                    break
                
                action = self.index[tag]
                result = action.__call__()

                #post processing 

                if tag == "policies":
                    result = ", ".join(result)
                elif tag == "secretsengine":
                    result = str(result)
                elif tag == "version":
                    result = False            
                break
        return result
