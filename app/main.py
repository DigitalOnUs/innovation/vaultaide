# compose_flask/app.py
from flask import Flask
from flask import request

# from keras.models import load_model
from slackclient import SlackClient
from lib.vault import Vault
from lib.github import Github
from lib.suggestions import Suggestions
from audit.server import AuditServer

from apscheduler.schedulers.background import BackgroundScheduler

from multiprocessing import Process

import os
import json
import pickle
import requests
import pymsteams
import atexit
import logging

##### common execution all environments ####
handlers=[logging.StreamHandler()] # add a file here if you want to save to file

logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)6d %(threadName)s %(message)s', handlers=handlers)
logging.info("INIT ...")

app = Flask(__name__)

host = '0.0.0.0'
port = 9090

addr = os.environ['VAULT_ADDR']
token = os.environ['VAULT_TOKEN']

vault = Vault(addr, token)
github = Github()
suggestions = Suggestions()

audit_server = AuditServer(host, port, suggestions, vault)
audit_process = Process(target=audit_server.serve)

audit_process.start()
logging.debug("VAULTAIDE *** [ New process spawned {}]".format(audit_process.pid))

scheduler = BackgroundScheduler()
scheduler.add_job(func=suggestions.suggest_version, trigger="interval", seconds=10)
scheduler.start()

##### end #####

# Scheduled Tasks
def schedule_suggestions(task, interval, time):
    interval_time = get_interval(interval)

    scheduler = BackgroundScheduler()

    if task == 1:
        scheduler.add_job(func=suggestions.suggest_version, trigger="interval", days=time)
    elif task == 2:
        scheduler.add_job(func=suggestions.adoption_stats, trigger="interval", days=time)
    elif task == 3:
        scheduler.add_job(func=suggestions.extant_leases, trigger="interval", days=time)
    elif task == 4:
        scheduler.add_job(func=suggestions.leases_ttl, trigger="interval", days=time)
    elif task == 5:
        scheduler.add_job(func=suggestions.unused_leases, trigger="interval", days=time)
    else:
        scheduler.add_job(func=suggestions.suggest_version, trigger="interval", days=time)
        scheduler.add_job(func=suggestions.adoption_stats, trigger="interval", days=time)
        scheduler.add_job(func=suggestions.extant_leases, trigger="interval", days=time)
        scheduler.add_job(func=suggestions.leases_ttl, trigger="interval", days=time)
        scheduler.add_job(func=suggestions.unused_leases, trigger="interval", days=time)

    scheduler.start()

# Http Endpoint
@app.route('/get-answer', methods=['POST'])
def get_answer():

    msg = request.form.get('msg', False)

    if msg.lower().find("metrics") >= 0:
        res = requests.get(
            addr + "/v1/sys/metrics?format=",
            headers={'X-Vault-Token': token}
        ).json()
    else:
        res = False


    if not res:
        return {'success': False, 'answer': ""}

    return {'success': True, 'answer': res}

# Slack Implementation
@app.route('/slack/get-answer', methods=['POST', 'GET'])
def slack_get_answer():
    # Slack bot token
    arr_token = ("xoxb", "918589458594", "931400580288", "9LrOqSiT1GEKFftbqrfRXhD4")
    sl_token = "-".join(arr_token)

    # Check if this request is a handshake
    if request.json.get('challenge', False):
        return {'challenge': request.json.get('challenge')}

    else:
        # Check if the last event was a bot response and it's from the right channel
        if not request.json['event'].get('bot_id', False):

            # Check if the message it's a channel join
            if request.json['event'].get('subtype', False) == "channel_join":
                res = "Welcome <@" + request.json['event'].get('user', "") + ">"

            # Get message
            elif request.json['event'].get('text', False):

                msg = request.json['event']['text']
                res = False

            else:
                # Unknown event
                res = False

            # send the message
            if res:
                slack_client = SlackClient(sl_token)
                raq = slack_client.api_call("chat.postMessage", channel=request.json['event']['channel'], text=res) # json.dumps(res, indent=4, sort_keys=True).strip("{").strip("}"))

        return {'success': True}

# Microsoft Teams Implementation
@app.route('/mt/get-answer', methods=['POST', 'GET'])
def mt_get_answer():
    msg = request.json.get('text', False)

    if msg:
        res = False
        webhook_url = "https://outlook.office.com/webhook/\
                66cbaada-14f1-4c2f-8639-22993cb92447@125b0b2a-\
                fc11-406b-8ee5-edc4003afbf3/IncomingWebhook/92b\
                01d770e4c44a6801c1a2062a9efaf/0dc1d84a-92f3-4bf4\
                -82aa-4a9b6316e748"

        # You must create the connectorcard object with the Microsoft Webhook URL
        myTeamsMessage = pymsteams.connectorcard(webhook_url)

        # Add text to the message.
        myTeamsMessage.text(res)

        # send the message.
        myTeamsMessage.send()

    return {'success': True}


'''
Debugging only the main statement only works
for non-production servers this is not nginx 
This code will not be executed in docker. 
'''
def http_server():
    app.run(host="0.0.0.0", debug=True, use_reloader=False)

if __name__ == '__main__':
    flask_process  = Process(target=http_server)
    flask_process.start()

atexit.register(lambda: scheduler.shutdown())
