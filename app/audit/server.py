import json
import logging
import time
import socket
import arrow

import logging

# common functions
def get_lease_id(data):
    if data['type'] == "request":
        return

    try:
        id = data["response"]["secret"]["lease_id"]
    except KeyError:
        id = None

    return id

class AuditServer(object):
    def __init__(self, host, port, suggestions, vault):
        self.host = host
        self.port = port
        self.suggestions = suggestions
        self.vault = vault

    def analize(self, data):
        for d in data.split(b'\n'):
            if not d:
                return
            j = json.loads(d)

            id = get_lease_id(j)

            if not id:
                return

            optimizable, time = used_time_greater_than_issued(id)
            if optimizable:
                message = "The expire time of the lease: {} is in {}".format(id, time)
                self.suggestions.leases_ttl(message)
    
    def used_time_greater_than_issued(self, id):
        response = self.vault.client.sys.read_lease(
            lease_id = id,
        )

        expire_time = arrow.get(response["data"]["expire_time"])
        issue_time = arrow.get(response["data"]["issue_time"])
        now = arrow.utcnow().to("local")

        remain_time = expire_time - now
        used_time = now - issue_time

        return remain_time > used_time, remain_time - used_time
    
    def serve(self):
        logging.info("***** Starting socket for AUDIT ********* ")
        print("**************** to listen the crap ....**********", flush=True)
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port))
            s.listen()
            conn, addr = s.accept()
            with conn:
                while True:
                    data = conn.recv(4096)
                    if not data:
                        break

                    self.analize(data)