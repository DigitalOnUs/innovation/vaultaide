# VaultAide

For demo purposes execute the following instructions


To create the bundle execute with a user in the docker group run

```
  docker-compose up
  docker-compose up --build
```

For testing with an existent vault cluster just build the standalone image

```
  docker build -t vault-aide .
```


Further details available [Wiki](https://gitlab.com/DigitalOnUs/innovation/vaultaide/-/wikis/home/)
