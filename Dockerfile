FROM tiangolo/uwsgi-nginx-flask:python3.7

COPY ./app /app

WORKDIR /app

RUN pip install -r requirements.txt
RUN mv /app/nginx/* /etc/nginx/conf.d/
RUN mv /app/certs/server.cert.pem /etc/ssl/certs/nginx-selfsigned.crt.pem 
RUN mv /app/certs/server.key.pem /etc/ssl/private/nginx-selfsigned.key.pem